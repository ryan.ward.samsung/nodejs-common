# Common 
Common classes and utilities for Assistant application

## Setup

### Localise.biz
You need an account with [localize.biz](https://localise.biz) to commit code to this repository

If you haven't done so already add the following to your `~/.bash_profile`

```bash
export TIMEROCKET_NODEJS_COMMON_LOCALISE_BIZ_API_KEY=your_api_key_goes_here
```

Reload your bash profile
```bash
source ~/.bash_profile
```

Verify your api key has been set as an environment variable
```bash
env | grep LOCALISE_BIZ_API_KEY
```

# install peers:
```bash
yarn install:peers
```