export class StringUtil {
  private constructor() {}

  public static isRegexString(str: string) {
    return str.substr(0, 1) === "/" && str.substr(str.length - 1, 1) === "/";
  }

  public static stringMatches(test: string, str: string) {
    const testTrimmed = test.trim();
    const strTrimmed = str.trim();
    if (test.trim() === str.trim()) {
      return true;
    }
    if (this.isRegexString(testTrimmed)) {
      if (
        new RegExp(testTrimmed.substr(1, testTrimmed.length - 2)).test(
          strTrimmed
        )
      ) {
        return true;
      }
    }
    return false;
  }
}
