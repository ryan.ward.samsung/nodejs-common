import { Injectable, LoggerService } from "@nestjs/common";
import {
  ClientInterface,
  Locale,
  LocaleI18nInterface,
  LocaleInterface,
  MessageContextInterface,
  MessageMetaInterface,
} from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { Context } from "./context";
import { v4 as uuidv4 } from "uuid";
import { LocaleUtil } from "../util";

@Injectable()
export class ContextBuilder {
  private locale: LocaleI18nInterface;
  private correlationId: string;
  private started: Date;

  constructor(
    private readonly logger: LoggerService,
    private readonly config: DefaultConfig,
    public client: ClientInterface,
    private readonly messageContext: MessageContextInterface,
    private i18nApi?: i18nAPI
  ) {}

  public build() {
    return new ContextBuilder(
      this.logger,
      this.config,
      { ...this.client },
      { ...this.messageContext },
      this.i18nApi
    );
  }

  public setI18nApi(i18nApi: i18nAPI) {
    this.i18nApi = i18nApi;
    return this;
  }

  public setMetaFromHeaders(headers: Record<string, string>) {
    this.setMetaFromHeadersForNewMessage(headers);
    if (headers["x-client-version"]) {
      this.client.version = headers["x-client-version"];
    }
    if (headers["x-client-id"]) {
      this.client.id = headers["x-client-id"];
    }
    if (headers["x-client-name"]) {
      this.client.name = headers["x-client-name"];
    }
    if (headers["x-client-variant"]) {
      this.client.variant = headers["x-client-variant"];
    }

    return this;
  }

  public setMetaFromHeadersForNewMessage(headers: Record<string, string>) {
    this.setCorrelationId(headers["x-correlation-id"]);
    this.setLocale(LocaleUtil.getLocaleFromHeaders(headers, this.getResult()));
    this.setStarted(headers["x-started"]);
    if (headers["x-context-category"]) {
      this.messageContext.category = headers["x-context-category"];
    }
    if (headers["x-context-id"]) {
      this.messageContext.id = headers["x-context-id"];
    }
    return this;
  }

  public setMeta(meta: MessageMetaInterface) {
    this.setCorrelationId(meta.correlationId);
    this.setLocale(new Locale(meta.locale.language, meta.locale.country));
    this.setStarted(meta.time.started);
    return this;
  }

  public setCorrelationId(correlationId: string): ContextBuilder {
    this.correlationId = correlationId;
    return this;
  }

  public setLocale(locale: LocaleInterface) {
    this.locale = new Locale(locale.language, locale.country);
    return this;
  }

  public setStarted(date: Date | string) {
    if (typeof date === "string") {
      this.started = new Date(date);
    } else {
      this.started = date;
    }
    return this;
  }

  private _getStarted(): Date {
    return this.started || new Date();
  }

  private _getCorrelationId(): string {
    return this.correlationId || uuidv4();
  }

  private _getLocale(): LocaleI18nInterface {
    return this.locale || new Locale("en", "US");
  }

  public getResult(): Context {
    return new Context(
      this._getCorrelationId(),
      this.logger,
      this.config,
      this.client,
      this._getLocale(),
      this.messageContext,
      this._getStarted(),
      this.i18nApi
    );
  }
}
