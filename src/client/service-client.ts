import { ClientInterface } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";

export class ServiceClient
  implements ClientInterface, JsonSerializableInterface<ClientInterface> {
  constructor(private readonly config: DefaultConfig) {}

  get id(): string {
    return this.config.clientId;
  }

  get name(): string {
    return this.config.appName;
  }

  get variant(): string {
    return this.config.environmentName;
  }

  get version(): string {
    return this.config.appVersion;
  }

  public toJSON(): ClientInterface {
    return {
      id: this.id,
      name: this.name,
      variant: this.variant,
      version: this.version,
    };
  }
}
