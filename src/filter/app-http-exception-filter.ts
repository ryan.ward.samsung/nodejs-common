import {
  ArgumentsHost,
  Catch,
  HttpException,
  HttpServer,
  HttpStatus,
  Injectable,
  LoggerService,
} from "@nestjs/common";
import { BaseExceptionFilter } from "@nestjs/core";
import { Request } from "express";
import { ErrorHttpResponse, ErrorMessage } from "../response";
import { FriendlyHttpException } from "../exception";
import { Locale } from "@timerocket/data-model";
import { i18nData } from "../locales/locales";
import { LocalesEnum } from "../locales/enum";
import { DefaultConfig } from "../config";

@Catch()
@Injectable()
export class AppHttpExceptionFilter extends BaseExceptionFilter {
  constructor(
    private readonly fallbackLocale: Locale,
    private readonly fallbackLogger: LoggerService,
    private readonly config: DefaultConfig,
    applicationRef?: HttpServer
  ) {
    super(applicationRef);
  }

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse();

    const logger =
      exception instanceof FriendlyHttpException
        ? exception.context.logger
        : this.fallbackLogger;

    logger.error({
      request: {
        headers: request.headers,
        body: request.body,
        url: request.url,
        method: request.method,
      },
      exception,
    });

    // If this is a normal error it means something random occurred and we want the stacktrace
    if (!(exception instanceof HttpException) && exception.stack) {
      logger.error(exception.stack);
    }

    const status =
      exception.getStatus !== undefined &&
      typeof exception.getStatus === "function"
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const locale =
      exception instanceof FriendlyHttpException
        ? exception.context.locale
        : this.fallbackLocale;

    const correlationId =
      exception instanceof FriendlyHttpException
        ? exception.context.correlationId
        : undefined;

    const started =
      exception instanceof FriendlyHttpException
        ? exception.context.started
        : undefined;

    let developerText = i18nData.__({
      phrase: LocalesEnum.UNKNOWN_ERROR,
      locale: locale.i18n,
    });
    if (exception) {
      if (exception.getStatus) {
        if (exception.message.message) {
          developerText = exception.message.message;
        } else {
          developerText = exception.message;
        }
      }
    }

    const userMessage =
      exception instanceof FriendlyHttpException ? exception.userMessage : "";

    const errorHttpResponse = new ErrorHttpResponse(
      status,
      locale,
      new ErrorMessage(
        locale,
        i18nData,
        null,
        null,
        developerText,
        userMessage
      ),
      exception.stack || null,
      this.config,
      correlationId,
      started
    );

    response.status(status).json(errorHttpResponse);
  }
}
