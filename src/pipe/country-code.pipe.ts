import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import { ValidCountryCodes } from "@timerocket/data-model";

@Injectable()
export class CountryCodePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!ValidCountryCodes.includes(value)) {
      throw new BadRequestException(`Invalid country code: ${value}`);
    }
    return value;
  }
}
