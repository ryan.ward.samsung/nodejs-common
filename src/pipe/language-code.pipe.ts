import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import { ValidLanguageCodes } from "@timerocket/data-model";

@Injectable()
export class LanguageCodePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!ValidLanguageCodes.includes(value)) {
      throw new BadRequestException(`Invalid language code: ${value}`);
    }
    return value;
  }
}
