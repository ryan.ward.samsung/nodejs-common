export interface JsonSerializableInterface<T> {
  toJSON(): T;
}
