import { InteractionInterface } from "@timerocket/data-model";

export class Interaction implements InteractionInterface {
  constructor(public readonly id: string, public readonly categoryId: string) {}
}
