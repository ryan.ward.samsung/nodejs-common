import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { Message } from "kafkajs";
import { ConsumerServiceDelegateInterface } from "./consumer-service-delegate.interface";
import { DefaultConfig } from "../../config";
import { EventHandlerFactoryInterface } from "../../event/event.handler.factory.interface";
import * as jsYaml from "js-yaml";
import * as fs from "fs";
import { TopicsConfigInterface } from "../../config/topics-config.interface";
import { EventHandler } from "../../event/event-handler";
import { ConsumerUtil } from "./consumer.util";
import { HealthzComponentEnum, HealthzService } from "../healthz";

// eslint-disable-next-line @typescript-eslint/no-var-requires
// tslint:disable-next-line:no-var-requires
const randomstring = require("randomstring");

@Injectable()
export class WebsocketConsumerService {
  private topics: TopicsConfigInterface;
  private eventHandler: EventHandler;

  constructor(
    private readonly healthzService: HealthzService,
    @Inject("CONSUMER_SERVICE_DELEGATE")
    private readonly consumerDelegateService: ConsumerServiceDelegateInterface,
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    @Inject("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")
    private readonly internalFactory: EventHandlerFactoryInterface
  ) {
    this.topics = jsYaml.safeLoad(
      fs.readFileSync(config.kafka.topicsConfigPath).toString()
    ) as TopicsConfigInterface;
    this.eventHandler = new EventHandler(logger);
  }

  async init(): Promise<void> {
    await this.healthzService.makeUnhealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );
    await this.consumerDelegateService.initializeConsumer(
      randomstring.generate()
    );
    await this.consumerDelegateService.connect();
    await this.healthzService.makeHealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );
  }

  async restart(topics: string[]) {
    try {
      await this.consumerDelegateService.stopConsumer();
      const finalTopics = ConsumerUtil.getTopics(topics);
      await this.consumerDelegateService.startConsumer(
        finalTopics,
        async (topic: string, message: Message) => {
          const handler = this.internalFactory.getHandler(topic);
          await this.eventHandler.handle(message, topic, handler);
        }
      );
    } catch (e) {
      await this.healthzService.makeUnhealthy(
        HealthzComponentEnum.MESSAGE_BROKER_KAFKA
      );
    }
  }
}
