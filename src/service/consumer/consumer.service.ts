import { Inject, Injectable, LoggerService } from "@nestjs/common";
import { Message } from "kafkajs";
import { ConsumerServiceDelegateInterface } from "./consumer-service-delegate.interface";
import { DefaultConfig } from "../../config";
import { EventHandlerFactoryInterface } from "../../event/event.handler.factory.interface";
import * as jsYaml from "js-yaml";
import * as fs from "fs";
import { TopicsConfigInterface } from "../../config/topics-config.interface";
import { EventHandler } from "../../event/event-handler";
import { StringUtil } from "../../util";
import { ConsumerUtil } from "./consumer.util";
import { HealthzComponentEnum, HealthzService } from "../healthz";
import { ConsumerTypeEnum } from "../../config";

// eslint-disable-next-line @typescript-eslint/no-var-requires
// tslint:disable-next-line:no-var-requires
const randomstring = require("randomstring");

@Injectable()
export class ConsumerService {
  private topics: TopicsConfigInterface;
  private eventHandler: EventHandler;

  constructor(
    private readonly healthzService: HealthzService,
    @Inject("CONSUMER_SERVICE_DELEGATE")
    private readonly consumerDelegateService: ConsumerServiceDelegateInterface,
    @Inject("CONFIG") private readonly config: DefaultConfig,
    @Inject("LOGGER") private readonly logger: LoggerService,
    @Inject("EXTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")
    private readonly externalFactory: EventHandlerFactoryInterface,
    @Inject("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")
    private readonly internalFactory: EventHandlerFactoryInterface,
    @Inject("CONSUMER_TYPE") private readonly consumerType: string
  ) {
    this.topics = jsYaml.safeLoad(
      fs.readFileSync(config.kafka.topicsConfigPath).toString()
    ) as TopicsConfigInterface;
    this.eventHandler = new EventHandler(logger);
    logger.debug(`Initializing consumer with type: ${consumerType}`);
  }

  async start(): Promise<void> {
    await this.healthzService.makeUnhealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );

    await this.consumerDelegateService.initializeConsumer(
      `${this.config.appName}-${this.consumerType}`
    );
    await this.consumerDelegateService.connect();

    await this.consumerDelegateService.startConsumer(
      this._getTopics(),
      async (topic: string, message: Message) => {
        await this.handle(topic, message);
      }
    );
    await this.healthzService.makeHealthy(
      HealthzComponentEnum.MESSAGE_BROKER_KAFKA
    );
  }

  private _getInternalTopics() {
    const internalTopicTypeMap = {
      [ConsumerTypeEnum.DEFAULT]: "internal",
      [ConsumerTypeEnum.GPU]: "gpu",
    };
    return this.topics.topics[internalTopicTypeMap[this.consumerType]];
  }

  private _getTopics() {
    let allTopics = [];
    if (this.consumerType === ConsumerTypeEnum.DEFAULT) {
      allTopics = allTopics.concat(this.topics.topics.external);
      allTopics = allTopics.concat(this.topics.topics.internal);
      this.logger.debug(
        `Subscribing to external topics: ${JSON.stringify(
          this.topics.topics.external
        )}`
      );
      this.logger.debug(
        `Subscribing to internal topics: ${JSON.stringify(
          this.topics.topics.internal
        )}`
      );
    }
    if (this.consumerType === ConsumerTypeEnum.GPU) {
      allTopics = allTopics.concat(this.topics.topics.gpu);
      this.logger.debug(
        `Subscribing to gpu topics: ${JSON.stringify(this.topics.topics.gpu)}`
      );
    }

    return ConsumerUtil.getTopics(allTopics);
  }

  public async handle(topic: string, message: any): Promise<void> {
    const handler = this._getFactory(topic).getHandler(topic);
    await this.eventHandler.handle(message, topic, handler);
  }

  public _getFactory(topic: string): EventHandlerFactoryInterface {
    for (const testTopic of this.topics.topics.external) {
      if (StringUtil.stringMatches(testTopic, topic)) {
        return this.externalFactory;
      }
    }
    for (const testTopic of this._getInternalTopics()) {
      if (StringUtil.stringMatches(testTopic, topic)) {
        return this.internalFactory;
      }
    }
    throw new Error(`Could not match any factory for topic: ${topic}`);
  }
}
