import { LoggerService } from "@nestjs/common";
import { MessageInterface } from "@timerocket/data-model";

export interface KafkaServiceInterface {
  startProducer(): Promise<any>;

  send(payload: MessageInterface<any>, logger: LoggerService): Promise<any>;
}
