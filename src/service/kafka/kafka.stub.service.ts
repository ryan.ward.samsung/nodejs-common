import { Injectable, LoggerService } from "@nestjs/common";
import { KafkaServiceInterface } from "./kafka-service.interface";
import { MessageInterface } from "@timerocket/data-model";

@Injectable()
export class KafkaStubService implements KafkaServiceInterface {
  public async startProducer(): Promise<any> {}

  public async send(
    payload: MessageInterface<any>,
    logger: LoggerService
  ): Promise<any> {}
}
