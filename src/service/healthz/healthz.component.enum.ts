export enum HealthzComponentEnum {
  DATABASE_REDIS = "database.redis",
  DATABASE_ELASTICSEARCH = "database.elasticsearch",
  DATABASE_MONGODB = "database.mongodb",
  MESSAGE_BROKER_KAFKA = "message-broker.kafka",
}
