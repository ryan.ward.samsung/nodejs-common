import { CustomLogger } from "./custom.logger";
import { Logger } from "@nestjs/common";
import { instance, mock, verify } from "ts-mockito";

describe("CustomLogger", () => {
  let loggerMock: Logger;
  let mockedLogger: Logger;

  beforeEach(() => {
    mockedLogger = mock(Logger);
    loggerMock = instance(mockedLogger);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Should allow the default context when no filtered contexts are provided", () => {
    const customLogger = new CustomLogger("info", [], true, loggerMock);
    customLogger.log("hello");
    verify(mockedLogger.log("hello", "info")).once();
  });

  it("Should allow the default context allowed filtered contexts is provided", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.log("hello");
    verify(mockedLogger.log("hello", "info")).once();
  });

  it("Should now allow the default context when filtered contexts does not include default context", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.log("hello");
    verify(mockedLogger.log("hello", "info")).never();
  });

  it("Should not allow custom context when filtered contexts does not include custom context", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.log("hello", "error");
    verify(mockedLogger.log("hello", "error")).never();
  });

  it("Should not allow error with a method when filtered contexts does not include error", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.error("hello", "", "");
    verify(mockedLogger.error("hello", "", "error")).never();
  });

  it("Should not allow debug with a method when filtered contexts does not include debug", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.debug("hello");
    verify(mockedLogger.log("hello", "debug")).never();
  });

  it("Should not allow info with a method when filtered contexts does not include info", () => {
    const customLogger = new CustomLogger("info", ["debug"], true, loggerMock);
    customLogger.info("hello");
    verify(mockedLogger.log("hello", "info")).never();
  });

  it("Should not allow verbose with a method when filtered contexts does not include verbose", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.verbose("hello");
    verify(mockedLogger.log("hello", "verbose")).never();
  });

  it("Should not allow warn with a method when filtered contexts does not include warn", () => {
    const customLogger = new CustomLogger("info", ["info"], true, loggerMock);
    customLogger.warn("hello");
    verify(mockedLogger.log("hello", "warn")).never();
  });

  it("Should allow debug with a method when filtered contexts does not include debug", () => {
    const customLogger = new CustomLogger(
      "info",
      ["debug", "info"],
      true,
      loggerMock
    );
    customLogger.debug("hello");
    verify(mockedLogger.log("hello", "debug")).once();
  });

  it("Should allow error when error is in filtered context", () => {
    const customLogger = new CustomLogger("info", ["error"], true, loggerMock);
    customLogger.error("hello", "");
    verify(mockedLogger.error("hello", "", "error")).once();
  });
});
