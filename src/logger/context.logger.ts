import { LoggerService } from "@nestjs/common";
import { v4 as uuidV4 } from "uuid";
import { DefaultConfig } from "../config";
import { ClientInterface } from "@timerocket/data-model";

export class ContextLogger implements LoggerService {
  private readonly stackTraceId: string;

  constructor(
    private readonly correlationId,
    private readonly config: DefaultConfig,
    private readonly client: ClientInterface,
    private readonly logger: LoggerService
  ) {
    this.stackTraceId = uuidV4();
  }

  public debug(message: any, context?: string): any {
    this.logger.debug(this._getData(message), context);
  }

  public error(message: any, trace?: string, context?: string): any {
    this.logger.error(this._getData(message), context);
  }

  public log(message: any, context?: string): any {
    this.logger.log(this._getData(message), context);
  }

  public verbose(message: any, context?: string): any {
    this.logger.verbose(this._getData(message), context);
  }

  public warn(message: any, context?: string): any {
    this.logger.warn(this._getData(message), context);
  }

  private _getData(message) {
    return {
      stackTraceId: this.stackTraceId,
      correlationId: this.correlationId,
      appVersion: this.config.appVersion,
      appName: this.config.appName,
      clientName: this.client.name,
      clientVersion: this.client.version,
      clientVariant: this.client.variant,
      message,
    };
  }
}
