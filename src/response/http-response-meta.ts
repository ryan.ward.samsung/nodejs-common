import { HttpStatus } from "@nestjs/common";
import {
  LocaleInterface,
  MessageMetaInterface,
  MetaTypeEnum,
} from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";

export class HttpResponseMeta
  extends MessageMeta
  implements MessageMetaInterface {
  constructor(
    public readonly status: HttpStatus,
    type: MetaTypeEnum,
    locale: LocaleInterface,
    config: DefaultConfig,
    correlationId: string,
    started: Date
  ) {
    super(type, locale, config, correlationId, started);
  }
}
