import { SqlHostConfigInterface } from "./sql-host-config.interface";

export interface SqlConfigInterface {
  client: string;
  database: string;
  userName: string;
  password: string;
  primary: SqlHostConfigInterface;
  reader: SqlHostConfigInterface;
}
