import { HostConfig } from "./host-config";
import { EndpointConfigInterface } from "./endpoint-config.interface";

export class EndpointConfig
  extends HostConfig
  implements EndpointConfigInterface {
  constructor(host, port: string) {
    super(host, port);
  }

  get endpoint() {
    return `${this.host}:${this.port}`;
  }
}
