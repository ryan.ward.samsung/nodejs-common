import { EndpointConfig } from "./endpoint-config";
import { ConfigBooleanEnum } from "./config-boolean.enum";
import { SASLMechanism } from "kafkajs";
import { KafkaConfigInterface } from "./kafka-config.interface";

export class KafkaConfig
  extends EndpointConfig
  implements KafkaConfigInterface {
  enabled: boolean;
  saslMechanism: SASLMechanism;

  constructor(
    enabled: string,
    public readonly host: string,
    port: string,
    public readonly username: string,
    public readonly password: string,
    saslMechanism: string,
    public readonly topicsConfigPath: string,
    public readonly clientId: string
  ) {
    super(host, port);
    this.saslMechanism = saslMechanism as SASLMechanism;
    this.enabled = enabled === ConfigBooleanEnum.TRUE;
  }
}
