import { UrlInterface } from "./url.interface";
import { PingConfigInterface } from "./ping-config.interface";

export class ElasticsearchConfig implements UrlInterface, PingConfigInterface {
  public readonly pingIntervalSeconds: number;
  constructor(public readonly url: string, pingIntervalSeconds: string) {
    this.pingIntervalSeconds = parseInt(pingIntervalSeconds, 10);
  }
}
