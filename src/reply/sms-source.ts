import {
  PhoneNumberChannelEnum,
  PhoneSmsDevice,
  SmsSourceInterface,
} from "@timerocket/data-model";

export class SmsSource implements SmsSourceInterface {
  constructor(
    public readonly sentToPhoneNumber: string,
    public readonly channel: PhoneNumberChannelEnum,
    public readonly device: PhoneSmsDevice
  ) {}
}
