import {
  InteractionInterface,
  MessageTypeEnum,
  PhoneNumberChannelEnum,
  ReplyDataInterface,
  SmsSourceInterface,
  SourceDestinationTypeEnum,
} from "@timerocket/data-model";
import { SmsSource } from "./sms-source";
import { v4 as uuidV4 } from "uuid";

export class SmsReply implements ReplyDataInterface<SmsSourceInterface> {
  public interaction: InteractionInterface;
  public readonly source: SmsSourceInterface;
  public readonly text: string;
  messageId: string;
  type: MessageTypeEnum;

  constructor(
    interaction: InteractionInterface,
    sentFrom: string,
    sentTo: string,
    channel: PhoneNumberChannelEnum,
    message: string
  ) {
    this.source = new SmsSource(sentTo, channel, {
      phoneNumber: sentFrom,
      type: SourceDestinationTypeEnum.PHONE_SMS,
    });
    this.text = message;
    this.messageId = uuidV4();
    this.type = MessageTypeEnum.TEXT;
  }
}
