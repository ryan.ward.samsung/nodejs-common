import { ClientInterface } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { JsonSerializableInterface } from "../message";
export declare class ServiceClient implements ClientInterface, JsonSerializableInterface<ClientInterface> {
    private readonly config;
    constructor(config: DefaultConfig);
    get id(): string;
    get name(): string;
    get variant(): string;
    get version(): string;
    toJSON(): ClientInterface;
}
