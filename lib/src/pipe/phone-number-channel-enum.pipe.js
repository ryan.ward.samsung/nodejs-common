"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneNumberChannelEnumPipe = void 0;
const common_1 = require("@nestjs/common");
const data_model_1 = require("@timerocket/data-model");
let PhoneNumberChannelEnumPipe = class PhoneNumberChannelEnumPipe {
    transform(value, metadata) {
        const validPhoneNumberChannels = Object.values(data_model_1.PhoneNumberChannelEnum);
        if (!validPhoneNumberChannels.includes(value)) {
            throw new common_1.BadRequestException(`Invalid phone number channel: ${value}`);
        }
        return value;
    }
};
PhoneNumberChannelEnumPipe = __decorate([
    common_1.Injectable()
], PhoneNumberChannelEnumPipe);
exports.PhoneNumberChannelEnumPipe = PhoneNumberChannelEnumPipe;
