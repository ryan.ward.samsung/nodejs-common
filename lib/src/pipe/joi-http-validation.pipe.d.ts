import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
import { ObjectSchema } from "joi";
export declare class JoiHttpValidationPipe implements PipeTransform {
    private schema;
    constructor(schema: ObjectSchema);
    transform(value: any, metadata: ArgumentMetadata): any;
}
