import { InteractionInterface } from "@timerocket/data-model";
export declare class Interaction implements InteractionInterface {
    readonly id: string;
    readonly categoryId: string;
    constructor(id: string, categoryId: string);
}
