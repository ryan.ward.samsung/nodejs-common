"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FriendlyHttpException = void 0;
const common_1 = require("@nestjs/common");
class FriendlyHttpException extends common_1.HttpException {
    constructor(response, context, userMessage, status) {
        super(response, status);
        this.context = context;
        this.userMessage = userMessage;
    }
}
exports.FriendlyHttpException = FriendlyHttpException;
