export interface TopicAuthorizorInterface {
    authorize(topic: string, token: string): Promise<boolean>;
}
