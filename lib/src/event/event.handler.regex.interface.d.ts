import { EventHandlerInterface } from "./event.handler.interface";
export interface EventHandlerRegexInterface {
    handler: EventHandlerInterface;
    regexp: RegExp;
}
