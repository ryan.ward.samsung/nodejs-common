import { Message } from "kafkajs";
import { EventHandlerInterface } from "./event.handler.interface";
import { LoggerService } from "@nestjs/common";
export declare class EventHandler {
    private readonly logger;
    constructor(logger: LoggerService);
    handle(message: Message, topic: string, handler: EventHandlerInterface): Promise<void>;
}
