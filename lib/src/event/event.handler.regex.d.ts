import { EventHandlerRegexInterface } from "./event.handler.regex.interface";
import { EventHandlerInterface } from "./event.handler.interface";
export declare class EventHandlerRegex implements EventHandlerRegexInterface {
    readonly handler: EventHandlerInterface;
    readonly regexp: RegExp;
    constructor(handler: EventHandlerInterface, regexp: RegExp);
}
