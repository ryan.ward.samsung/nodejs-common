"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventHandler = void 0;
class EventHandler {
    constructor(logger) {
        this.logger = logger;
    }
    async handle(message, topic, handler) {
        let data;
        try {
            data = JSON.parse(message.value.toString());
            await handler.handle(data, topic);
        }
        catch (e) {
            this.logger.error(`Error handling event: ${JSON.stringify(data)} from topic ${topic}`, e.stack);
        }
    }
}
exports.EventHandler = EventHandler;
