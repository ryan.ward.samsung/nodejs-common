import { PhoneNumberChannelEnum, PhoneSmsDevice, SmsSourceInterface } from "@timerocket/data-model";
export declare class SmsSource implements SmsSourceInterface {
    readonly sentToPhoneNumber: string;
    readonly channel: PhoneNumberChannelEnum;
    readonly device: PhoneSmsDevice;
    constructor(sentToPhoneNumber: string, channel: PhoneNumberChannelEnum, device: PhoneSmsDevice);
}
