"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmsSource = void 0;
class SmsSource {
    constructor(sentToPhoneNumber, channel, device) {
        this.sentToPhoneNumber = sentToPhoneNumber;
        this.channel = channel;
        this.device = device;
    }
}
exports.SmsSource = SmsSource;
