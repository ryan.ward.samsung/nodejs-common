/// <reference types="i18n" />
import { LoggerService } from "@nestjs/common";
import { ClientInterface, LocaleInterface, MessageContextInterface, MessageMetaInterface } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { Context } from "./context";
export declare class ContextBuilder {
    private readonly logger;
    private readonly config;
    client: ClientInterface;
    private readonly messageContext;
    private i18nApi?;
    private locale;
    private correlationId;
    private started;
    constructor(logger: LoggerService, config: DefaultConfig, client: ClientInterface, messageContext: MessageContextInterface, i18nApi?: i18nAPI);
    build(): ContextBuilder;
    setI18nApi(i18nApi: i18nAPI): this;
    setMetaFromHeaders(headers: Record<string, string>): this;
    setMetaFromHeadersForNewMessage(headers: Record<string, string>): this;
    setMeta(meta: MessageMetaInterface): this;
    setCorrelationId(correlationId: string): ContextBuilder;
    setLocale(locale: LocaleInterface): this;
    setStarted(date: Date | string): this;
    private _getStarted;
    private _getCorrelationId;
    private _getLocale;
    getResult(): Context;
}
