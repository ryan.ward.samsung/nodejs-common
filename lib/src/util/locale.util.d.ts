import { Locale } from "@timerocket/data-model";
import { Context } from "../context";
export declare class LocaleUtil {
    private constructor();
    static getLocaleFromHeaders(headers: Record<string, string>, context: Context): Locale;
}
