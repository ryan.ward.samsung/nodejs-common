import { HttpStatus } from "@nestjs/common";
import { LocaleInterface, MessageMetaInterface, MetaTypeEnum } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";
export declare class HttpResponseMeta extends MessageMeta implements MessageMetaInterface {
    readonly status: HttpStatus;
    constructor(status: HttpStatus, type: MetaTypeEnum, locale: LocaleInterface, config: DefaultConfig, correlationId: string, started: Date);
}
