import { ErrorMessageInterface } from "@timerocket/data-model";
export interface ErrorMessageDataInterface {
    message: ErrorMessageInterface;
    stack: string;
}
