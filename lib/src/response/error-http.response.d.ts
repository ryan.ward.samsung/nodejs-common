import { HttpStatus } from "@nestjs/common";
import { ErrorMessageInterface, LocaleInterface, MessageInterface, MessageMetaInterface } from "@timerocket/data-model";
import { DefaultConfig } from "../config";
import { ErrorMessageDataInterface } from "./error-message-data.interface";
import { JsonSerializableInterface } from "../message";
export declare class ErrorHttpResponse implements JsonSerializableInterface<MessageInterface<ErrorMessageDataInterface>> {
    private readonly errorMessage;
    private readonly stack;
    readonly meta: MessageMetaInterface;
    constructor(status: HttpStatus, locale: LocaleInterface, errorMessage: ErrorMessageInterface, stack: string, config: DefaultConfig, correlationId?: string, started?: Date);
    toJSON(): MessageInterface<ErrorMessageDataInterface>;
    get data(): ErrorMessageDataInterface;
}
