"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorHttpResponse = void 0;
const data_model_1 = require("@timerocket/data-model");
const http_response_meta_1 = require("./http-response-meta");
class ErrorHttpResponse {
    constructor(status, locale, errorMessage, stack, config, correlationId, started) {
        this.errorMessage = errorMessage;
        this.stack = stack;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, data_model_1.MetaTypeEnum.NA_HTTP_HEALTHZ, locale, config, correlationId, started);
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
    get data() {
        return {
            message: this.errorMessage,
            stack: process.env.NODE_ENV !== "production" ? this.stack : null,
        };
    }
}
exports.ErrorHttpResponse = ErrorHttpResponse;
