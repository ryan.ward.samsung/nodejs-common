import { HttpStatus } from "@nestjs/common";
import { MessageInterface, MessageMetaInterface, MetaTypeEnum } from "@timerocket/data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
export declare class RestResponse implements JsonSerializableInterface<MessageInterface<string>> {
    readonly data: any;
    readonly meta: MessageMetaInterface;
    constructor(context: Context, status: HttpStatus, type: MetaTypeEnum, data: any);
    toJSON(): MessageInterface<any>;
}
