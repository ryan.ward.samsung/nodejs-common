"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestResponse = void 0;
const http_response_meta_1 = require("./http-response-meta");
class RestResponse {
    constructor(context, status, type, data) {
        this.data = data;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, type, context.locale, context.config, context.correlationId, context.started);
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
}
exports.RestResponse = RestResponse;
