export interface StatsdConfigInterface {
    enabled: boolean;
    serverPrefix: string;
    config: {
        host: string;
        port: number;
    };
}
