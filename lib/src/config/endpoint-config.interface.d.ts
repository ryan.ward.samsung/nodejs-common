import { HostConfigInterface } from "./host-config.interface";
export interface EndpointConfigInterface extends HostConfigInterface {
    endpoint: string;
}
