"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticatedEndpointConfig = void 0;
const endpoint_config_1 = require("./endpoint-config");
class AuthenticatedEndpointConfig extends endpoint_config_1.EndpointConfig {
    constructor(host, port, username, password) {
        super(host, port);
        this.username = username;
        this.password = password;
    }
    get endpoint() {
        return `${this.host}:${this.port}`;
    }
}
exports.AuthenticatedEndpointConfig = AuthenticatedEndpointConfig;
