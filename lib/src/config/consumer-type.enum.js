"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConsumerTypeEnum = void 0;
var ConsumerTypeEnum;
(function (ConsumerTypeEnum) {
    ConsumerTypeEnum["GPU"] = "gpu";
    ConsumerTypeEnum["DEFAULT"] = "default";
})(ConsumerTypeEnum = exports.ConsumerTypeEnum || (exports.ConsumerTypeEnum = {}));
