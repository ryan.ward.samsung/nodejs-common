"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultConfig = void 0;
const metrics_host_config_1 = require("./metrics-host-config");
const fs = require("fs");
const dotenv_1 = require("dotenv");
const kafka_config_1 = require("./kafka-config");
const jwt_config_1 = require("./jwt-config");
const common_1 = require("@nestjs/common");
const elasticsearch_config_1 = require("./elasticsearch-config");
const consumer_type_enum_1 = require("./consumer-type.enum");
let DefaultConfig = class DefaultConfig {
    constructor(basePath, appPath, fallbackEnvironment) {
        this.setup(basePath, appPath, process.env.ENV_NAME || fallbackEnvironment || "");
        this._appPath = appPath;
    }
    toJSON() {
        return {
            fallbackLanguage: this.fallbackLanguage,
            fallbackCountry: this.fallbackCountry,
            appVersion: this.appVersion,
            appName: this.appName,
            clientId: this.clientId,
            apiVersion: this.apiVersion,
            appPrefix: this.appPrefix,
            docsPrefix: this.docsPrefix,
            docsEnabled: this.docsEnabled,
            docsPath: this.docsPath,
            kafka: this.kafka,
            environmentName: this.environmentName,
            logLevels: this.logLevels,
            httpPort: this.httpPort,
            metrics: this.metrics,
            jwt: this.jwt,
            healthzFilePath: this.healthzFilePath,
            elasticsearch: this.elasticsearch,
            consumerType: this.consumerType,
            gpuEnabled: this.gpuEnabled,
            dataDirectory: this.dataDirectory,
        };
    }
    setup(basePath, appPath, environment) {
        const envBasePath = `${basePath}/env`;
        const configPath = `${envBasePath}/${environment}.env`;
        if (fs.existsSync(configPath)) {
            dotenv_1.config({ path: configPath });
        }
        const secretsPath = `${envBasePath}/${environment}${environment ? "." : ""}secrets.env`;
        if (fs.existsSync(secretsPath)) {
            const envConfig = dotenv_1.parse(fs.readFileSync(secretsPath));
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        const configOverridePath = `${envBasePath}/${environment}.override.env`;
        if (fs.existsSync(configOverridePath)) {
            const envConfig = dotenv_1.parse(fs.readFileSync(configOverridePath));
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        const packageJson = require(`${appPath}/package.json`);
        this._appName = packageJson.name;
        this._packageVersion = packageJson.version;
    }
    get fallbackLanguage() {
        return process.env.LOCALE_LANGUAGE_FALLBACK;
    }
    get fallbackCountry() {
        return process.env.LOCALE_COUNTRY_FALLBACK;
    }
    get appVersion() {
        return process.env.APP_VERSION || this._packageVersion;
    }
    get appName() {
        return this._appName;
    }
    get clientId() {
        return "dafae812-6a07-4e4f-8ac5-d5a81bb32cab";
    }
    get apiVersion() {
        return process.env.API_VERSION;
    }
    get appPrefix() {
        return process.env.APP_PREFIX;
    }
    get docsPrefix() {
        return process.env.DOCS_PREFIX;
    }
    get docsEnabled() {
        return process.env.DOCS_ENABLED === "true";
    }
    get docsPath() {
        return process.env.DOCS_PATH || "";
    }
    get kafka() {
        return new kafka_config_1.KafkaConfig(process.env.KAFKA_ENABLED, process.env.KAFKA_HOST, process.env.KAFKA_PORT, process.env.KAFKA_USERNAME, process.env.KAFKA_PASSWORD, process.env.SASL_MECHANISM, process.env.KAFKA_TOPICS_CONFIG_PATH ||
            `${this._appPath}/config/topics.yaml`, process.env.KAFKA_CLIENT_ID || process.env.POD_NAME || this.appName);
    }
    get environmentName() {
        return process.env.ENV_NAME;
    }
    get logLevels() {
        return process.env.LOG_LEVELS.trim().split(",");
    }
    get httpPort() {
        return parseInt(process.env.HTTP_PORT, 10);
    }
    get metrics() {
        return new metrics_host_config_1.MetricsHostConfig(process.env.GRAPHITE_HOST, process.env.GRAPHITE_PORT, process.env.METRICS_ENABLED);
    }
    get jwt() {
        return new jwt_config_1.JwtConfig(process.env.JWT_VERIFY, process.env.JWT_AUD);
    }
    get healthzFilePath() {
        return process.env.HEALTHZ_FILE_PATH || "/tmp/healthz";
    }
    get elasticsearch() {
        return new elasticsearch_config_1.ElasticsearchConfig(process.env.ELASTICSEARCH_URL, process.env.ELASTICSEARCH_PING_INTERVAL_SECONDS || "10");
    }
    get consumerType() {
        return (process.env.CONSUMER_TYPE ||
            consumer_type_enum_1.ConsumerTypeEnum.DEFAULT);
    }
    get gpuEnabled() {
        return process.env.GPU_ENABLED === "true";
    }
    get dataDirectory() {
        return process.env.DATA_DIRECTORY || "/data";
    }
};
DefaultConfig = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [String, String, String])
], DefaultConfig);
exports.DefaultConfig = DefaultConfig;
