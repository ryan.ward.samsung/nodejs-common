"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionConfig = void 0;
const endpoint_config_1 = require("./endpoint-config");
class ConnectionConfig extends endpoint_config_1.EndpointConfig {
    get connectionUrl() {
        return `${this.protocol}://${this.endpoint}`;
    }
}
exports.ConnectionConfig = ConnectionConfig;
