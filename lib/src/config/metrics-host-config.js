"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MetricsHostConfig = void 0;
const host_config_1 = require("./host-config");
const config_boolean_enum_1 = require("./config-boolean.enum");
class MetricsHostConfig extends host_config_1.HostConfig {
    constructor(host, port, enabled) {
        super(host, port);
        this.host = host;
        this.enabled = enabled === config_boolean_enum_1.ConfigBooleanEnum.TRUE;
    }
}
exports.MetricsHostConfig = MetricsHostConfig;
