"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageMetaFromMeta = void 0;
const data_model_1 = require("@timerocket/data-model");
class MessageMetaFromMeta {
    constructor(context, meta, type) {
        this.type = type;
        this.client = context.client;
        this.context = context.messageContext;
        this.correlationId = meta.correlationId;
        this.locale = meta.locale;
        this.schemaVersion = data_model_1.Version.CURRENT;
        this.time = {
            started: meta.time.started,
            created: new Date().toISOString(),
        };
    }
}
exports.MessageMetaFromMeta = MessageMetaFromMeta;
