import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { HealthzService } from "./healthz";
export declare class ElasticsearchHealthzService {
    private readonly config;
    private readonly logger;
    private readonly elasticsearchService;
    private readonly healthzService;
    constructor(config: DefaultConfig, logger: LoggerService, elasticsearchService: ElasticsearchService, healthzService: HealthzService);
    start(): Promise<void>;
    waitForHealthy(): Promise<void>;
    private _ping;
}
