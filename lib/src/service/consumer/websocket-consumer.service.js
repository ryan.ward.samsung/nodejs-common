"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebsocketConsumerService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("../../config");
const jsYaml = require("js-yaml");
const fs = require("fs");
const event_handler_1 = require("../../event/event-handler");
const consumer_util_1 = require("./consumer.util");
const healthz_1 = require("../healthz");
const randomstring = require("randomstring");
let WebsocketConsumerService = class WebsocketConsumerService {
    constructor(healthzService, consumerDelegateService, config, logger, internalFactory) {
        this.healthzService = healthzService;
        this.consumerDelegateService = consumerDelegateService;
        this.config = config;
        this.logger = logger;
        this.internalFactory = internalFactory;
        this.topics = jsYaml.safeLoad(fs.readFileSync(config.kafka.topicsConfigPath).toString());
        this.eventHandler = new event_handler_1.EventHandler(logger);
    }
    async init() {
        await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        await this.consumerDelegateService.initializeConsumer(randomstring.generate());
        await this.consumerDelegateService.connect();
        await this.healthzService.makeHealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
    }
    async restart(topics) {
        try {
            await this.consumerDelegateService.stopConsumer();
            const finalTopics = consumer_util_1.ConsumerUtil.getTopics(topics);
            await this.consumerDelegateService.startConsumer(finalTopics, async (topic, message) => {
                const handler = this.internalFactory.getHandler(topic);
                await this.eventHandler.handle(message, topic, handler);
            });
        }
        catch (e) {
            await this.healthzService.makeUnhealthy(healthz_1.HealthzComponentEnum.MESSAGE_BROKER_KAFKA);
        }
    }
};
WebsocketConsumerService = __decorate([
    common_1.Injectable(),
    __param(1, common_1.Inject("CONSUMER_SERVICE_DELEGATE")),
    __param(2, common_1.Inject("CONFIG")),
    __param(3, common_1.Inject("LOGGER")),
    __param(4, common_1.Inject("INTERNAL_DOMAIN_EVENT_HANDLER_FACTORY")),
    __metadata("design:paramtypes", [healthz_1.HealthzService, Object, config_1.DefaultConfig, Object, Object])
], WebsocketConsumerService);
exports.WebsocketConsumerService = WebsocketConsumerService;
