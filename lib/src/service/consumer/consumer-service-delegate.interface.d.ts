export interface ConsumerServiceDelegateInterface {
    connect(): Promise<void>;
    startConsumer(topics: (string | RegExp)[], callback: (topic: string, message: any) => Promise<void>): Promise<void>;
    initializeConsumer(consumerGroup: string): Promise<void>;
    stopConsumer(): Promise<void>;
}
