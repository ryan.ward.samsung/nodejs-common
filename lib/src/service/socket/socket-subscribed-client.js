"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketSubscribedClient = void 0;
const common_1 = require("@nestjs/common");
class SocketSubscribedClient {
    constructor(token, client, authorizors) {
        this.token = token;
        this.client = client;
        this.authorizors = authorizors;
        this.topics = [];
    }
    async subscribeToTopics(topics) {
        for (const topic of topics) {
            if (!(await this._isAuthorized(topic))) {
                this.client.close(1000, `${common_1.HttpStatus.FORBIDDEN}: Forbidden`);
                throw new common_1.UnauthorizedException(new Error(`Not allowed to subscribe to topic '${topic}'`));
            }
        }
        this.topics = topics;
    }
    getTopics() {
        return this.topics;
    }
    async _isAuthorized(topic) {
        for (const authorizor of this.authorizors) {
            if (await authorizor.authorize(topic, this.token)) {
                return true;
            }
        }
        return false;
    }
    forwardFromTopic(topic, message) {
        if (this.topics.includes(topic)) {
            this.client.send(message);
        }
    }
}
exports.SocketSubscribedClient = SocketSubscribedClient;
