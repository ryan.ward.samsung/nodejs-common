"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaLogger = void 0;
const kafkajs_1 = require("kafkajs");
class KafkaLogger {
    constructor(logger) {
        this.logger = logger;
    }
    toContext(level) {
        switch (level) {
            case kafkajs_1.logLevel.ERROR:
            case kafkajs_1.logLevel.NOTHING:
                return "error";
            case kafkajs_1.logLevel.WARN:
                return "warn";
            case kafkajs_1.logLevel.DEBUG:
                return "debug";
            default:
                return "info";
        }
    }
    log({ level, log }) {
        const logJson = JSON.stringify(log);
        const logObject = JSON.parse(logJson);
        let finalLevel = level;
        if (level === kafkajs_1.logLevel.ERROR) {
            if (logObject.error === "The group is rebalancing, so a rejoin is needed") {
                finalLevel = kafkajs_1.logLevel.INFO;
            }
        }
        this.logger.log(logJson, this.toContext(finalLevel));
    }
}
exports.KafkaLogger = KafkaLogger;
