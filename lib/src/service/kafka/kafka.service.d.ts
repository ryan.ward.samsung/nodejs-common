import { LoggerService } from "@nestjs/common";
import { Message } from "kafkajs";
import { DefaultConfig } from "../../config";
import { ConsumerServiceDelegateInterface } from "../consumer";
export declare class KafkaService implements ConsumerServiceDelegateInterface {
    private readonly logger;
    private _kafka;
    private _kafkaConsumer;
    private _kafkaProducer;
    private _kafkaAdmin;
    constructor(config: DefaultConfig, logger: LoggerService);
    stopConsumer(): Promise<void>;
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    initializeConsumer(consumerGroup: string): Promise<void>;
    initializeProducer(): Promise<void>;
    startConsumer(topics: (string | RegExp)[], callback: (topic: string, message: Message) => Promise<void>): Promise<void>;
    private _ensureTopicsExist;
    publish(topic: string, payload: any): Promise<void>;
    publishBulk(topic: string, payloads: any[]): Promise<void>;
}
