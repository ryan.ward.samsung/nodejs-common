"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContextLogger = void 0;
const uuid_1 = require("uuid");
class ContextLogger {
    constructor(correlationId, config, client, logger) {
        this.correlationId = correlationId;
        this.config = config;
        this.client = client;
        this.logger = logger;
        this.stackTraceId = uuid_1.v4();
    }
    debug(message, context) {
        this.logger.debug(this._getData(message), context);
    }
    error(message, trace, context) {
        this.logger.error(this._getData(message), context);
    }
    log(message, context) {
        this.logger.log(this._getData(message), context);
    }
    verbose(message, context) {
        this.logger.verbose(this._getData(message), context);
    }
    warn(message, context) {
        this.logger.warn(this._getData(message), context);
    }
    _getData(message) {
        return {
            stackTraceId: this.stackTraceId,
            correlationId: this.correlationId,
            appVersion: this.config.appVersion,
            appName: this.config.appName,
            clientName: this.client.name,
            clientVersion: this.client.version,
            clientVariant: this.client.variant,
            message,
        };
    }
}
exports.ContextLogger = ContextLogger;
