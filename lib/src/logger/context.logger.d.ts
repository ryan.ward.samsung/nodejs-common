import { LoggerService } from "@nestjs/common";
import { DefaultConfig } from "../config";
import { ClientInterface } from "@timerocket/data-model";
export declare class ContextLogger implements LoggerService {
    private readonly correlationId;
    private readonly config;
    private readonly client;
    private readonly logger;
    private readonly stackTraceId;
    constructor(correlationId: any, config: DefaultConfig, client: ClientInterface, logger: LoggerService);
    debug(message: any, context?: string): any;
    error(message: any, trace?: string, context?: string): any;
    log(message: any, context?: string): any;
    verbose(message: any, context?: string): any;
    warn(message: any, context?: string): any;
    private _getData;
}
