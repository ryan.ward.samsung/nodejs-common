"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomLogger = void 0;
const common_1 = require("@nestjs/common");
let CustomLogger = class CustomLogger {
    constructor(defaultContext = "info", filterContext = [], isTimestampEnabled, logger) {
        this.defaultContext = defaultContext;
        this.filterContext = filterContext;
        if (logger) {
            this.logger = logger;
        }
        else {
            this.logger = new common_1.Logger(defaultContext, {
                timestamp: isTimestampEnabled,
            });
        }
    }
    log(message, context = "") {
        if (context === "") {
            context = this.defaultContext;
        }
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes(context)) {
                this.logger.log(message, context);
            }
        }
        else {
            this.logger.log(message, context);
        }
    }
    error(error, trace, context = "error") {
        let message;
        let useTrace;
        if (error instanceof Error) {
            message = error.message;
            useTrace = error.stack;
        }
        else {
            message = error;
            useTrace = trace;
        }
        if (this.filterContext.length > 0) {
            if (this.filterContext.includes(context)) {
                this.logger.error(message, useTrace, context);
            }
        }
        else {
            this.logger.error(message, useTrace, context);
        }
    }
    debug(message, context = "debug") {
        this.log(message, context);
    }
    verbose(message, context = "verbose") {
        this.log(message, context);
    }
    warn(message, context = "warn") {
        this.log(message, context);
    }
    info(message, context = "info") {
        this.log(message, context);
    }
};
CustomLogger = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [String, Array, Boolean, Object])
], CustomLogger);
exports.CustomLogger = CustomLogger;
